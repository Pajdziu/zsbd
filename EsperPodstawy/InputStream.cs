﻿namespace EsperPodstawy
{
    #region

    using System;
    using System.Globalization;
    using System.Text;

    using EsperPodstawy.Misc;

    #endregion

    public class InputStream
    {
        #region Fields
        // DATA_ROZPOCZECIA
        private const string StartDate = "2001-09-05";

        // DATA_ZAKONCZENIA
        private const string EndDate = "2001-09-20";

        // MAX_LICZBA_BLEDOW
        private const int MaxFailures = 5;

        // tablicaInformacjioPlikach
        private readonly StockFileInfo[] stockFiles;
        #endregion

        public InputStream()
        {
            this.stockFiles = new[]
                                  {
                                      new StockFileInfo("files/tableAPPLE_NASDAQ.csv", "Apple", "NASDAQ"),
                                      new StockFileInfo("files/tableCOCACOLA_NYSE.csv", "CocaCola", "NYSE"),
                                      new StockFileInfo("files/tableDISNEY_NYSE.csv", "Disney", "NYSE"),
                                      new StockFileInfo("files/tableFORD_NYSE.csv", "Ford", "NYSE"),
                                      new StockFileInfo("files/tableGOOGLE_NASDAQ.csv", "Google", "NASDAQ"),
                                      new StockFileInfo("files/tableHONDA_NYSE.csv", "Honda", "NYSE"),
                                      new StockFileInfo("files/tableIBM_NASDAQ.csv", "IBM", "NASDAQ"),
                                      new StockFileInfo("files/tableINTEL_NASDAQ.csv", "Intel", "NASDAQ"),
                                      new StockFileInfo("files/tableMICROSOFT_NASDAQ.csv", "Microsoft", "NASDAQ"),
                                      new StockFileInfo("files/tableORACLE_NASDAQ.csv", "Oracle", "NASDAQ"),
                                      new StockFileInfo("files/tablePEPSICO_NYSE.csv", "PepsiCo", "NYSE"),
                                      new StockFileInfo("files/tableYAHOO_NASDAQ.csv", "Yahoo", "NASDAQ")
                                  };
        }

        // generuj()
        public void Generate()
        {
            DateTime startDate;
            DateTime endDate;
            int failuresCount = 0;

            try
            {
                startDate = DateTime.Parse(StartDate);
                endDate = DateTime.Parse(EndDate);
            }
            catch
            {
                Console.Error.WriteLine("Nie udało się wczytać podanych dat rozpoczęcia i zakończenia!");
                throw;
            }


            foreach (var stockFile in this.stockFiles)
            {
                foreach (var line in stockFile.ReverseLineReader)
                {
                    try
                    {
                        string[] splitResult = line.Split(',');

                        if (splitResult[0].Equals("Date"))
                        {
                            continue;
                        }

                        DateTime quotationDate = DateTime.Parse(splitResult[0]);

                        if (quotationDate < startDate || quotationDate > endDate)
                        {
                            continue;
                        }

                        StockPrice stockPrice = new StockPrice(
                            stockFile.CorporationName,
                            stockFile.MarketName,
                            quotationDate,
                            float.Parse(splitResult[1], CultureInfo.InvariantCulture),
                            float.Parse(splitResult[2], CultureInfo.InvariantCulture),
                            float.Parse(splitResult[3], CultureInfo.InvariantCulture),
                            float.Parse(splitResult[4], CultureInfo.InvariantCulture),
                            float.Parse(splitResult[5], CultureInfo.InvariantCulture));

                        Console.WriteLine(stockPrice.ToString());
                    }
                    catch (Exception ex)
                    {
                        failuresCount++;
                        Console.Error.WriteLine("Błąd parsowania! Linia: [line]; Po raz: " + failuresCount);

                        if (failuresCount >= MaxFailures)
                        {
                            Console.Error.WriteLine("Zbyt wiele błędów!");
                            throw;
                        }
                    }
                }
            }
        }

        // InformacjeOPliku
        private class StockFileInfo
        {
            public StockFileInfo(string fileName, string corporationName, string marketName)
            {
                this.FileName = fileName;
                this.CorporationName = corporationName;
                this.MarketName = marketName;
                this.ReverseLineReader = new ReverseLineReader(fileName, Encoding.UTF8);
            }

            #region Properties
            // nazwaPliku
            public string FileName { get; private set; }

            // nazwaSpolki
            public string CorporationName { get; private set; }

            // nazwaMarketu
            public string MarketName { get; private set; }

            public ReverseLineReader ReverseLineReader { get; set; }
            #endregion
        }
    }
}
